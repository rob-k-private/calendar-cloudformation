"""Configures all tasks to run with invoke."""

from invoke import task
import glob
import os
import fnmatch


@task(
    default=True,
    help={
        'warnings': 'Warning configuration, as described at https://docs.python.org/2/using/cmdline.html#cmdoption-W \
        for example, to disable Deprecation',
        'filename': 'Path to template(s) to `compile`. Supports globbing.',
    },
)
def build(ctx, warnings='once::DeprecationWarning', filename=None):
    """Build all templates."""
    print("Building all templates")
    import sys
    import subprocess
    import inspect
    if filename is not None:
        templates = [x for x in glob.glob(filename)]
    else:
        os.chdir(os.path.dirname(os.path.abspath(inspect.stack()[0][1])))
        templates = [x for x in glob.glob('templates/*') if x[-3:] == '.py']

    rv = 0
    for template in templates:
        print(" + Executing {0}".format(template))
        if subprocess.call([sys.executable, '-W{0}'.format(warnings), '{0}'.format(template)]) != 0:
            rv = 1
    exit(rv)


@task(help={
    'docs': "Remove generated docs",
    'verbose': "Show which files are being removed.",
    'compiled': 'Also clean up compiled python files.',
})
def clean(ctx, verbose=False, compiled=False):
    """Clean up all output files."""
    command = "rm -rvf {files}" if verbose else "rm -rf {files}"

    patterns = ['output/*.json', 'output/*/*.json']
    if compiled is True:
        for root, dirnames, filenames in os.walk('.'):
            for filename in fnmatch.filter(filenames, '*.pyc'):
                patterns.append(os.path.join(root, filename))

    for pattern in patterns:
        ctx.run(command.format(files=pattern))


@task(
    aliases=["flake8", "pep8"],
    help={
        'filename': 'File(s) to lint. Supports globbing.',
    },
)
def lint(ctx, filename=None):
    """Run flake8 python linter."""
    command = 'flake8'
    if filename is not None:
        templates = [x for x in glob.glob(filename)]
        command += ' ' + " ".join(templates)

    ctx.run(command)


@task(
    help={
        'filename': 'File(s) to lint. Supports globbing.',
    },
)
def validate(ctx, filename=None):
    """Validate the output file(s)."""
    exclude = ['bcm_mapping', 'ec2_profile']
    command = 'aws-cfn-validate '
    if filename is not None:
        command += '{filename} '.format(filename=filename)
    else:
        command += 'output/*/*.json '

    command += ' '.join(['--exclude {0}'.format(x) for x in exclude])
    ctx.run(command)
